using System.Collections.Generic;
using UnityEngine;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ImgprocModule;
using System.Linq;
using OpenCVForUnity.UnityUtils;
using System;

public static class ZMathExtend
{

    /// <summary>
    /// This structure stores a single Delaunay triangle.
    /// </summary>
    public struct Triangle
    {
        /// <summary>
        /// The first triangle corner. 
        /// </summary>
        public Point P1;

        /// <summary>
        /// The second triangle corner. 
        /// </summary>
        public Point P2;

        /// <summary>
        /// The third triangle corner. 
        /// </summary>
        public Point P3;

        /// <summary>
        /// Construct a new Triangle instance.
        /// </summary>
        /// <param name="p1">The first triangle corner.</param>
        /// <param name="p2">The second triangle corner.</param>
        /// <param name="p3">The third triangle corner.</param>
        public Triangle(Point p1, Point p2, Point p3)
        {
            P1 = p1;
            P2 = p2;
            P3 = p3;
        }

        /// <summary>
        /// Convert the triangle to an array of Point2f structures, due to Unity has Vector2, we use Vector2 instead.
        /// </summary>
        /// <returns>An array of Point2f structures corresponding to each triangle corner</returns>
        public Point[] ToPointArray()
        {
            return new Point[]
            {
                new Point(P1.x, P1.y),
                new Point(P2.x, P2.y),
                new Point(P3.x, P3.y)
            };
        }
    }

    /// <summary>
    /// This structure stores a single warp from one triangle to another.
    /// </summary>
    public struct Warp
    {
        /// <summary>
        /// The source triangle to warp from.
        /// </summary>
        public Triangle Source;

        /// <summary>
        /// The destination triangle to warp to.
        /// </summary>
        public Triangle Destination;

        /// <summary>
        /// Construct a new Warp instance.
        /// </summary>
        /// <param name="source">The source triangle to warp from.</param>
        /// <param name="destination">The destination triangle to warp to.</param>
        public Warp(Triangle source, Triangle destination)
        {
            Source = source;
            Destination = destination;
        }
    }

    /// <summary>
    /// Perform Delaunay triangulation on a given set of landmark points.
    /// </summary>
    /// <param name="points">The landmark points to use for triangulation.</param>
    /// <returns>A list of Triangle structures that each refer to a single triangle of landmark points.</returns>
    public static List<Triangle> GetDelaunayTriangles(List<Point> points)
    {
        if(points == null)
        {
            Debug.Log("Cannot find delaunay triangle due to no point input");
            return null;
        }

        //The list of delauney triangle as a result
        var result = new List<Triangle>();

        //Calculate the bounding box around all the point given
        var rect = FindRect(points);

        //Inflate the rect with width/height given, allow fast calculation.
        rect.inflate(10, 10);

        //List triangle is a matrix contain 6 values each row, each 2 value in a row determine each vertex of a triangle. 
        //So we convert to list of float where each 6 elements will present a triangle.
        MatOfFloat6 triangles = new MatOfFloat6();
        List<float> trianglesPoints = new List<float>();

        //The Subdiv2D class handles Delaunay triangulation
        var subdiv = new Subdiv2D(rect);

        //For each point, we insert into subdivision           
        foreach (var p in points)
        {
            var point = new Point(p.x, p.y);
            subdiv.insert(point);
        }

        //Then get delauney triangle list based on the list point was inserted.
        //And convert to list of point value,
        subdiv.getTriangleList(triangles);
        trianglesPoints = triangles.toList();

        //Now, we recontructing new triangle based on list of point value.
        //For each 6 value in list
        for (int i = 0; i < trianglesPoints.Count; i += 6)
        {
            //We contruct 3 point, each point is a vertice of a triangle.
            Point p1 = new Point(trianglesPoints[i], trianglesPoints[i + 1]);
            Point p2 = new Point(trianglesPoints[i + 2], trianglesPoints[i + 3]);
            Point p3 = new Point(trianglesPoints[i + 4], trianglesPoints[i + 5]);

            //Then create new triangle
            Triangle triangle = new Triangle(p1, p2, p3);

            //And add to list of result.
            result.Add(triangle);
        }

        //Return result as an enumeration of triangle structs
        return result;
    }

    /// <summary>
    /// Calculate the warps between the source and destination landmark points
    /// </summary>
    /// <param name="sourcePoints">The landmark points in the source image</param>
    /// <param name="destPoints">The landmark points in the destination image</param>
    /// <param name="destTriangles">The Delaunay triangles in the destination image</param>
    /// <returns>An enumeration of Warp structs that describe how to warp the source image to the destination image</returns>
    public static IEnumerable<Warp> GetWarps(List<Point> sourcePoints, List<Point> destPoints, IEnumerable<Triangle> destTriangles)
    {
        //Build lists of source and destination landmark points
        var sourceList = sourcePoints;
        var destList = destPoints;

        //Find all three triangle points in the list of destination landmark points
        var indices = from t in destTriangles
                      let p1 = destPoints.First(p => Mathf.Abs((float)p.x - (float)t.P1.x) < 1 && Mathf.Abs((float)p.y - (float)t.P1.y) < 1)
                      let p2 = destPoints.First(p => Mathf.Abs((float)p.x - (float)t.P2.x) < 1 && Mathf.Abs((float)p.y - (float)t.P2.y) < 1)
                      let p3 = destPoints.First(p => Mathf.Abs((float)p.x - (float)t.P3.x) < 1 && Mathf.Abs((float)p.y - (float)t.P3.y) < 1)
                      select new
                      {
                          X1 = destList.IndexOf(p1),
                          X2 = destList.IndexOf(p2),
                          X3 = destList.IndexOf(p3)
                      };

        //Return enumeration of warps from source to destination triangles
        var warp =  from x in indices
               select new Warp(
                   new Triangle(sourceList[x.X1], sourceList[x.X2], sourceList[x.X3]),
                   new Triangle(destList[x.X1], destList[x.X2], destList[x.X3]));
        if(warp.Any())
            Debug.Log($"Find warp complete, warp info: {warp.ToList().Count}");

        return warp;
    }

    /// <summary>
    /// Apply the given warps to a specified image and return the warped result.
    /// </summary>
    /// <param name="sourceImage">The source image to warp</param>
    /// <param name="width">The width of the destination image</param>
    /// <param name="height">The height of the destination image</param>
    /// <param name="warps">The warps to apply</param>
    /// <returns>The warped image</returns>
    public static Mat ApplyWarps(Texture2D sourceImage, int width, int height, IEnumerable<Warp> warps)
    {
        //Set up opencv images for the replacement image and the output
        var source = new Mat(sourceImage.height, sourceImage.width, CvType.CV_8UC3);
        Utils.texture2DToMat(sourceImage, source);

        //The output matrix.
        var destination = new Mat(height, width, CvType.CV_8UC3);
        destination.setTo(new Scalar(0));

        //Process all warps
        foreach (var warp in warps)
        {
            var t1 = warp.Source.ToPointArray();
            var t2 = warp.Destination.ToPointArray();

            // get bounding rects around source and destination triangles
            var r1 = FindRect(t1.ToList());
            var r2 = FindRect(t2.ToList());

            //Crop the input image to r1
            Mat img1Cropped = new Mat(r1.size(), source.type());
            new Mat(source, r1).copyTo(img1Cropped);

            //Adjust triangles to local coordinates within their bounding box
            for (int i = 0; i < t1.Length; i++)
            {
                t1[i].x -= r1.x;
                t1[i].y -= r1.y;
                t2[i].x -= r2.x;
                t2[i].y -= r2.y;
            }

            //Convert to Matrix of point, each point x,y value is a float instead of double
            //We did it because getAffineTransfrom of wrapper class only accept this.
            MatOfPoint2f matOfPoint2F1 = new MatOfPoint2f(t1);
            MatOfPoint2f matOfPoint2F2 = new MatOfPoint2f(t2);

            //Get the transformation matrix to warp t1 to t2
            var transformMatrix = Imgproc.getAffineTransform(matOfPoint2F1, matOfPoint2F2);

            // warp triangle
            var img2Cropped = new Mat(r2.height, r2.width, img1Cropped.type());
            Imgproc.warpAffine(img1Cropped, img2Cropped, transformMatrix, img2Cropped.size(), Imgproc.INTER_NEAREST, Core.BORDER_CONSTANT);

            //Create a mask in the shape of the t2 triangle
            var hull = from p in t2 select new Point(p.x, p.y);
            var mask = new Mat(r2.height, r2.width, CvType.CV_8UC3);
            mask.setTo(new Scalar(0));

            //Conver hull to hull point matrix
            MatOfPoint hullPoint = new MatOfPoint();
            hullPoint.fromList(hull.ToList());

            //Fill convex poly.
            Imgproc.fillConvexPoly(mask, hullPoint, new Scalar(1, 1, 1), 8, 0);

            //Alpha-blend the t2 triangle - this sets all pixels outside the triangle to zero
            Core.multiply(img2Cropped, mask, img2Cropped);

            //Cut the t2 triangle out of the destination image
            var target = new Mat(destination, r2);
            Core.multiply(target, new Scalar(1, 1, 1) - mask, target);

            //Insert the t2 triangle into the destination image
            Core.add(target, img2Cropped, target);
        }

        //Return the destination image
        return destination;
    }

    /// <summary>
    /// Find bounding rectangle of set of point.
    /// </summary>
    /// <param name="pointSet">The set of point need to find rectangle.</param>
    /// <returns>The rect contain all point in set and no rotation applied.</returns>
    public static OpenCVForUnity.CoreModule.Rect FindRect(List<Point> pointSet)
    {
        //Let define max is maximum point value can reach and min is minium value can reach
        //We find two point, the lowest point in x axis and highest point in y axis.
        double xMin = double.MaxValue, yMin = double.MaxValue, xMax = double.MinValue, yMax = double.MinValue;

        //Find mininum x value, y value and maximum x value y value
        for (var i = 0; i < pointSet.Count; i++)
        {
            if (xMin > pointSet[i].x)
                xMin = pointSet[i].x;
            if (xMax < pointSet[i].x)
                xMax = pointSet[i].x;

            if (yMin > pointSet[i].y)
                yMin = pointSet[i].y;
            if (yMax < pointSet[i].y)
                yMax = pointSet[i].y;
        }

        //Contruct the new top left point and bottom right point
        Point topLeft = new Point(xMin, yMax);
        Point bottomRight = new Point(xMax, yMin);

        //Create rect based on two point
        OpenCVForUnity.CoreModule.Rect rect = new OpenCVForUnity.CoreModule.Rect(bottomRight, topLeft);

        //Return the rect
        return rect;
    }

    /// <summary>
    /// Find 3 or 4 corner point of a Rect following order bottom left, top left, bottom right.
    /// </summary>
    /// <param name="rect">The rect covering area.</param>
    /// <returns>Return list of point following order.</returns>
    public static List<Point> FindCorner(OpenCVForUnity.CoreModule.Rect rect, bool isFindAll = false)
    {
        List<Point> points = new List<Point>();

        Point topLeft = new Point(rect.x, rect.y + rect.height);
        Point bottomLeft = new Point(rect.x, rect.y);
        Point bottomRight = new Point(rect.x + rect.width, rect.y);
        Point topRight = new Point(rect.x + rect.width, rect.y + rect.height);

        points.Add(bottomLeft); 
        points.Add(topLeft); 
        points.Add(bottomRight);

        if (isFindAll)
            points.Add(topRight);

        return points;
    }

    /// <summary>
    /// This function using to convert opencv Point to unity Vector2
    /// </summary>
    /// <param name="point">Open cv point</param>
    /// <returns>Return the Unity point as Vector2</returns>
    public static Vector2 ConvertPointToVector2(Point point)
    {
        return new Vector2((float)point.x, (float)point.y);
    }

    /// <summary>
    /// This function using to convert unity Vector2 to opencv Point
    /// </summary>
    /// <param name="vect"></param>
    /// <returns></returns>
    public static Point ConvertVector2ToPoint(Vector2 vect)
    {
        return new Point(vect.x, vect.y);
    }

    /// <summary>
    /// This function using to convert Unity rect to OpenCv rect
    /// </summary>
    /// <param name="rect">Unity rectangle get from RectTransform</param>
    /// <returns>OpenCV rectangle.</returns>
    public static OpenCVForUnity.CoreModule.Rect ConvertRectToRect(UnityEngine.Rect rect)
    {
        int x = Screen.width / 2 + (int)rect.x;
        int y = Screen.height / 2 + (int)rect.y;
        int width = (int)rect.width;
        int height = (int)rect.height;

        return new OpenCVForUnity.CoreModule.Rect(x , y, width, height);
    }

    public struct ConvexityDefect
    {
        public int startPoint;
        public int endPoint;
        public int farestPoint;
        public float approxDistFarestPoint;
    }

    public static List<ConvexityDefect> Convert2ConvexityDefect(MatOfInt4 convexityDefects)
    {
        if(convexityDefects == null || convexityDefects.width() * convexityDefects.height() < 1)
        {
            Debug.Log("ZMathExtend: Convexity defects input is null");
            return null;
        }
        
        int[] pointIndex = convexityDefects.toArray();
        List<ConvexityDefect> convexityDefectsList = new List<ConvexityDefect>();

        for (int i = 0; i < pointIndex.Length; i += 4)
        {
            ConvexityDefect conDef = new ConvexityDefect
            {
                startPoint = pointIndex[i],
                endPoint = pointIndex[i+1],
                farestPoint = pointIndex[i+2],
                approxDistFarestPoint = pointIndex[i+3]
            };

            convexityDefectsList.Add(conDef);
        }

        return convexityDefectsList;
    }

    class MeshGrid
    {
        readonly int[,,] _data;

        private int D => _data.GetLength(0);
        private int R => _data.GetLength(1);
        private int C => _data.GetLength(2);

        public MeshGrid(int[,,] data)
        {
            _data = data;
        }

        public MeshGrid(Tuple<int, int> x, Tuple<int, int> y)
        {
            int startX = x.Item1;
            int rows = x.Item2;
            int startY = y.Item1;
            int cols = y.Item2;
            _data = new int[2, rows, cols];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    _data[0, i, j] = startX + i;
                    _data[1, i, j] = startY + j;
                }
            }
        }

        public MeshGrid Transpose()
        {
            var data = new int[C, R, D];
            for (int i = 0; i < D; i++)
            {
                for (int j = 0; j < R; j++)
                {
                    for (int k = 0; k < C; k++)
                    {
                        data[k, j, i] = _data[i, j, k];
                    }
                }
            }

            return new MeshGrid(data);
        }

        static int[] Flatten(int[,,] src)
        {
            var d = src.GetLength(0);
            var r = src.GetLength(1);
            var c = src.GetLength(2);
            var res = new int[d * r * c];
            for (int i = 0; i < d; i++)
            {
                for (int j = 0; j < r; j++)
                {
                    for (int k = 0; k < c; k++)
                    {
                        res[i * r * c + j * c + k] = src[i, j, k];
                    }
                }
            }

            return res;
        }

        public int[,] Reshape(int rows, int cols)
        {
            if (rows == -1) rows = _data.Length / cols;
            var data = new int[rows, cols];
            var data1d = Flatten(_data);
            Console.WriteLine(data1d);
            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    data[r, c] = data1d[r * cols + c];
                }
            }

            return data;
        }

        public void Print()
        {
            for (int i = 0; i < D; i++)
            {
                Console.Write("[");
                for (int j = 0; j < R; j++)
                {
                    Console.Write("[ ");
                    for (int k = 0; k < C; k++)
                    {
                        Console.Write(_data[i, j, k] + " ");
                    }

                    Console.Write("]");
                    Console.WriteLine();
                }

                Console.Write("]");
                Console.WriteLine();
            }
        }

        public static void PrintArr(int[,] arr)
        {
            Console.Write("[ ");
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                Console.Write("[ ");
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.Write(arr[i, j] + " ");
                }

                Console.Write("]");
                Console.WriteLine();
            }

            Console.Write("]");
        }

        public static void BroadCast(int[,] src, int[,] dst)
        {
            for (int i = 0; i < dst.GetLength(0); i++)
            {
                for (int j = 0; j < dst.GetLength(1); j++)
                {
                    Console.WriteLine($"{i}=={j}");
                    src[i, j] = dst[i, j];
                }
            }
        }
    }
}
