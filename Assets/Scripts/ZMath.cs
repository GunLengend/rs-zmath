﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ZMath
{
    /// <summary>
    /// This function return the next value in a circle of bound. Using for find next element in a list but 
    /// it return to the beginning when reach to the bound
    /// </summary>
    /// <param name="value">The current value</param>
    /// <param name="bound">The maximum value, current value no longer go over it.</param>
    /// <returns></returns>
    public static int ValueInCircle(int value, int bound)
    {
        return (value + 1) % bound;
    }

    /// <summary>
    /// Calculate the width of the bound box around the object by included the bound of children object.
    /// Warning: It's will count any object have Renderer component even it is particle system.
    /// </summary>
    /// <param name="go">Game object need to calculate the width</param>
    /// <returns></returns>
    public static float Width(GameObject go)
    {
        Quaternion currentRotation = go.transform.rotation;
        go.transform.rotation = Quaternion.Euler(0f, 0f, 0f);

        Bounds bounds = new Bounds(go.transform.position, Vector3.zero);

        foreach (Renderer renderer in go.GetComponentsInChildren<Renderer>())
        {
            //Excluded the AOHighlight particle on calculating.
            if (renderer.GetComponent<ParticleSystem>() == null)
            {
                //Excluded the FOV Mesh on calculating.
                if (renderer.tag != "FOV")
                {
                    bounds.Encapsulate(renderer.bounds);
                }
            }
        }

        Vector3 localCenter = bounds.center - go.transform.position;
        bounds.center = localCenter;

        go.transform.rotation = currentRotation;

        return 2 * Vector3.Distance(bounds.center, bounds.max);
    }

    /// <summary>
    /// This function using to find contact point from the position to the direction by casting a ray to hit the layer.
    /// The reduce offset given the reduce distance from collision point to the opposite of the direction.
    /// </summary>
    /// <param name="position">The position to start casting a ray</param>
    /// <param name="direction">The direction of the ray</param>
    /// <param name="reduceOffset">The distance reduce from the collision point to the position</param>
    /// <param name="layer">The layer which the ray will trigger when hit</param>
    /// <returns></returns>
    public static Vector3 FindContactPoint(Vector3 position, Vector3 direction, float reduceOffset, LayerMask layer)
    {
        Vector3 hitPoint = Vector3.zero;

        //Create ray to check in direction of the object.
        Ray checkRay = new Ray(position, direction);

        //If in direction hit something.
        if (Physics.Raycast(checkRay, out RaycastHit rayHit, 10f, layer))
        {
            //The hitpoint now become shorter by subtract with reduce offset, eg. reduce size of object.           
            hitPoint = FindPointInDirection(rayHit.point, -direction, reduceOffset);
        }

        return hitPoint;
    }

    /// <summary>
    /// Return a point on a sphere with a radius around object
    /// </summary>
    /// <param name="center">A position of the object</param>
    /// <param name="radius">A distance from center of sphere to the point around</param>
    /// <returns>Return a point on a sphere around object</returns>
    public static Vector3 RandomOnSphere(Vector3 center, float radius)
    {
        float u = UnityEngine.Random.Range(0, 1);
        float v = UnityEngine.Random.Range(0, 1);

        float theta = 2 * Mathf.PI * u;
        float phi = Mathf.Acos(2 * v - 1);

        float x = center.x + (radius * Mathf.Sin(phi) * Mathf.Cos(theta));
        float y = center.y + (radius * Mathf.Sin(phi) * Mathf.Sin(theta));
        float z = center.z + (radius * Mathf.Cos(phi));

        return new Vector3(x, y, z);
    }

    /// <summary>
    /// Return a point on a circle with a radius around object.
    /// </summary>
    /// <param name="center">A position of the object</param>
    /// <param name="radius">A distance from object to the point on the circle</param>
    /// <returns>Return a point on a circle around object</returns>
    public static Vector3 RandomOnCircle(Vector3 center, float radius)
    {
        //Get a point in unit circle on 2d plane
        Vector2 pointOnCircle = UnityEngine.Random.insideUnitCircle.normalized;

        Vector3 result = new Vector3(pointOnCircle.x, 0, pointOnCircle.y) * radius + center;
        result.y = center.y;

        //Convert to 3d world space at object postion plus with radius.
        return result;
    }

    /// <summary>
    /// Return a point randomly in a circle with a radius around object
    /// </summary>
    /// <param name="center">A position of the object</param>
    /// <param name="radius">A distance from object to the point in the circle</param>
    /// <returns></returns>
    public static Vector3 RandomInCircle(Vector3 center, float radius)
    {
        //Get a point in unit circle on 2d plane
        Vector2 pointOnCircle = UnityEngine.Random.insideUnitCircle;

        Vector3 result = new Vector3(pointOnCircle.x, 0, pointOnCircle.y) * radius + center;
        result.y = center.y;

        //Convert to 3d world space at object postion plus with radius.
        return result;
    }

    /// <summary>
    /// Return a new Vector3 point from old position with distance offset in a direction input.
    /// Ex: Get a position 10 meter away from current postion in front of me.
    /// </summary>
    /// <param name="position">The position from</param>
    /// <param name="direction">The direction we need to find new point</param>
    /// <param name="distance">How far from the position to the new one</param>
    /// <returns></returns>
    public static Vector3 FindPointInDirection(Vector3 position, Vector3 direction, float distance)
    {
        return position + direction.normalized * distance;
    }

    /// <summary>
    /// Return the inital velocity need to launch from initial position to target postion with maximum height.
    /// </summary>
    /// <param name="height">How tall the object need to go over</param>
    /// <param name="gravityFactor">The factor using to increase gravity, make speed of object go faster</param>
    /// <param name="initPos">The initital position</param>
    /// <param name="targetPos">The target position</param>
    /// <returns>The initial velocity formed v0 or u in math and physic</returns>
    public static Vector3 CalculateInitalVelocity(float height, Vector3 initPos, Vector3 targetPos, float gravityFactor = 1f)
    {
        //Calculate gravity based on user input
        float gravity = -9.81f * gravityFactor;

        //Modify current physics system to new gravity
        Physics.gravity = Vector3.up * gravity;

        //The offset height between target pos and init pos, called Py
        float displacementY = targetPos.y - initPos.y;

        //Because we calculate in 3D space then using Vector instead of some float, vector = scalar * direction, called Px
        Vector3 displacementXZ = new Vector3(targetPos.x - initPos.x, 0, targetPos.z - initPos.z);

        //vY0 = Squrt(-2gh), we find vector vY0 with it scalar
        Vector3 velocityY = Vector3.up * Mathf.Sqrt(-2 * gravity * height);

        float time = Mathf.Sqrt(-2 * height / gravity) + Mathf.Sqrt(2 * (displacementY - height) / gravity);

        //vX0 = Px / Squrt(-2h/g) + Squrt(2(Py-h)/g) = s/t
        Vector3 velocityXZ = displacementXZ / time;

        //Return the v0 velocity
        return velocityY * -Mathf.Sign(gravity) + velocityXZ;
    }

    /// <summary>
    /// Return the initial velocity need to launch from inital position to target position with angles from 0 < angle < 90 
    /// </summary>
    /// <param name="angle">Which angle the object will be launch</param>
    /// <param name="initPos">The initital position</param>
    /// <param name="targetPos">The target position</param>
    /// <param name="gravityFactor">The factor using to increase gravity, make speed of object go faster</param>
    /// <returns></returns>
    public static Vector3 CalculateInitialVelocity(float angle, Vector3 initPos, Vector3 targetPos, float gravityFactor = 1f)
    {
        //Calculate gravity based on user input
        float gravity = 9.81f * gravityFactor;

        //Modify current physics system to new gravity
        Physics.gravity = Vector3.up * -gravity;

        if (angle < 0 && angle > 90)
            throw new Exception("Angle must be 0 < angle < 90");

        //The offset height between init pos and target pos, called Py
        float displacementY = initPos.y - targetPos.y;

        //Px, the plane in x and z axis cause we calculating everything in 3D
        Vector3 displacementXZ = new Vector3(targetPos.x - initPos.x, 0, targetPos.z - initPos.z);

        //Distance between two object
        float distance = displacementXZ.magnitude;

        //Current angle between a and b
        var angleBetween = -Mathf.Atan2(displacementY, distance);

        Debug.Log(angleBetween * Mathf.Rad2Deg);

        //Convert Euler's angle to radian.
        angle *= Mathf.Deg2Rad;

        //The initial v0, see: https://physics.stackexchange.com/questions/27992/solving-for-initial-velocity-required-to-launch-a-projectile-to-a-given-destinat
        float v0 = (1 / Mathf.Cos(angle)) * Mathf.Sqrt((0.5f * gravity * Mathf.Pow(distance, 2)) / (distance * Mathf.Tan(angle) + displacementY));

        //Convert v0 to vector, find the velocity vector in plane YZ
        Vector3 initVelocity = new Vector3(0, v0 * Mathf.Sin(angle), v0 * Mathf.Cos(angle));

        //Find angle in plane XZ to determine how many angle we need to rotation to face to object.
        float angleBetweenObjects = Vector3.Angle(Vector3.forward, displacementXZ) * (targetPos.x > initPos.x ? 1 : -1);

        //Rotation the v0 vector to the target. 
        return Quaternion.AngleAxis(angleBetweenObjects, Vector3.up) * initVelocity;
    }
}
