using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZBall : MonoBehaviour
{
    public GameObject Target;
    public GameObject Root;
    private Rigidbody Ball;
    
    // Start is called before the first frame update
    void Start()
    {
        Target = GameObject.FindGameObjectWithTag("Target");
        Root = GameObject.FindGameObjectWithTag("Root");
        Ball = GetComponent<Rigidbody>();

        var v0 = CalculateInitialVelocity(30f, Root.transform.position, Target.transform.position, 1f);
        Ball.velocity = v0;
    }

    private Vector3 CalculateInitialVelocity(float angle, Vector3 initPos, Vector3 targetPos, float gravityFactor =1f)
    {
        float gravity = 9.81f * gravityFactor;
        Physics.gravity = Vector3.up * -gravity;

        float displacementY = initPos.y - targetPos.y;

        Vector3 displacementXZ = new Vector3(targetPos.x - initPos.x, 0, targetPos.z - initPos.z);
        float distance = displacementXZ.magnitude;

        angle *= Mathf.Deg2Rad;

        float v0 = (1/ Mathf.Cos(angle)) * Mathf.Sqrt((0.5f * gravity * Mathf.Pow(distance,2)) / (distance * Mathf.Tan(angle) + displacementY));

        Vector3 initVelocity = new Vector3(0, v0 * Mathf.Sin(angle), v0 * Mathf.Cos(angle));

        float angleBetweenObject = Vector3.Angle(Vector3.forward, displacementXZ) * (targetPos.x > initPos.x ? 1 : -1);
        return Quaternion.AngleAxis(angleBetweenObject, Vector3.up) * initVelocity;
    }
}
