using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZShoot : MonoBehaviour
{
    public GameObject Ball;
    private float spawnTime;

    private void Update()
    {
        if(Time.time >= spawnTime)
        {
            spawnTime = Time.time + 0.2f;
            Instantiate(Ball, transform.position, Quaternion.identity);
        }

    }
}
