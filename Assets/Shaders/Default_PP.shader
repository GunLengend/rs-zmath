﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Phoenix/DefaultPP"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
		_OutlineColor ("Outline Color", Color) = (0,0,0,1)
    }

    SubShader
    {
        Tags
        { 
            "Queue"="Transparent" 
            "IgnoreProjector"="True" 
            "RenderType"="Transparent" 
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Fog { Mode Off }
        //Blend One OneMinusSrcAlpha
        ZTest Always 
        //ZWrite Off 
        Blend SrcAlpha OneMinusSrcAlpha
		Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile DUMMY PIXELSNAP_ON
            #include "UnityCG.cginc"

            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float4 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                fixed4 texcoord  : TEXCOORD0;
            };

            fixed4 _Color;
			
			uniform float4 _OutlineColor;
            v2f vert(appdata_t IN)
            {
                v2f OUT;
                OUT.vertex = UnityObjectToClipPos(IN.vertex);

				
				float pixelgreen=5.0;
				OUT.vertex.xz*= 1+(pixelgreen/320);
				OUT.vertex.yz*= 1+(pixelgreen/240);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color * _Color;
                #ifdef PIXELSNAP_ON
                OUT.vertex = UnityPixelSnap (OUT.vertex);
                #endif

                return OUT;
            }

            sampler2D _MainTex;

            fixed4 frag(v2f IN) : COLOR
            {
                fixed4 c = tex2D(_MainTex, IN.texcoord) * IN.color;
                    c=_OutlineColor;
                    c.a = 1;
                fixed py = IN.texcoord.y / IN.texcoord.w;
					if(py*240/1 > 224)
					{
					c.a = 0;
					}
				return c;
            }

            int Random (int min, int max)
			{
			     float cap = max - min;
			     int rand;
			     return rand;
			}
        ENDCG
        }

        Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile DUMMY PIXELSNAP_ON
            #include "UnityCG.cginc"

            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float4 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                fixed4 texcoord  : TEXCOORD0;
            };

            fixed4 _Color;

            v2f vert(appdata_t IN)
            {
                v2f OUT;
                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color * _Color;
                #ifdef PIXELSNAP_ON
                OUT.vertex = UnityPixelSnap (OUT.vertex);
                #endif

                return OUT;
            }

            sampler2D _MainTex;

            fixed4 frag(v2f IN) : COLOR
            {
                fixed4 c = tex2D(_MainTex, IN.texcoord) * IN.color;
                c.rgb *= c.a;
                fixed p = IN.texcoord.y / IN.texcoord.w;
                if((int)(p*240/1 > 224)) {
                    c.a = 0;
                }
				return c;
            }

            int Random (int min, int max)
			{
			     float cap = max - min;
			     int rand;
			     return rand;
			}
        ENDCG
        }
    }
}